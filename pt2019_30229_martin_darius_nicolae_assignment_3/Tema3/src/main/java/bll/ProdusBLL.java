package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import bll.validators.Validator;
import dao.ProdusDAO;
import model.Client;
import model.Comanda;
import model.Produs;

public class ProdusBLL {
	private List<Validator<Produs>> validators;
	private ProdusDAO produsDAO;

	public ProdusBLL() {
		validators = new ArrayList<Validator<Produs>>();
		produsDAO = new ProdusDAO();
	}

	public Produs findProdusById(int id) {
		Produs st = produsDAO.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The produs with id =" + id + " was not found!");
		}
		return st;
	}
	
	public void insertProdus(Produs c) {
		Produs st=produsDAO.insert(c);
		
	}
	 public void updateProdus(Produs c,int id) {
		   Produs st=produsDAO.update(c, id);
	   }
	   
	   public void deleteProdus(Produs c,int id) {
		   Produs st=produsDAO.delete(c, id);
	   }
	   
	   public List<Produs> findAllProducts() {
		   List<Produs> lista=produsDAO.findAll();
		   return lista;
	   }
}
