package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import bll.ClientBLL;
import bll.ComandaBLL;
import bll.ProdusBLL;
import model.Client;
import model.Comanda;
import model.Produs;

import start.Start;

public class Controller {
	protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());
	   private ClientBLL m_client;
	   private ComandaBLL m_comanda;
	   private ProdusBLL m_produs;
	   private View m_view;
	   
	   public int idClient;
	   public String numeClient;
	   public String adresaClient;
	   public String emailClient;
	   public int varstaClient;
	   public int baniClient;
	   
	   public int idProdus;
	   public String numeProdus;
	   public int cantitateProdus;
	   public int pretProdus;
	   
	   public int id;
	   public int idClientCom;
	   public int idProdusCom;
	   public int cantitate;
	   
	 public Controller(ClientBLL client, ComandaBLL comanda, ProdusBLL produs, View view ) {
		 m_client=client;
		 m_comanda=comanda;
		 m_produs=produs;
		 m_view=view;
		 
		 m_view.butonClienti(new butonClientiListener());
		 m_view.butonProdus(new butonProdusListener());
		 m_view.butonComenzi(new butonComenziListener());
		 
		 m_view.insertClient(new insertClientListener());
		 m_view.deleteClient(new deleteClientListener());
		 m_view.selectClient(new selectClientListener());
		 m_view.updateClient(new updateClientListener());
		 
		 m_view.insertProdus(new insertProdusListener());
		 m_view.deleteProdus(new deleteProdusListener());
		 m_view.selectProdus(new selectProdusListener());
		 m_view.updateProdus(new updateProdusListener());
		 
		 m_view.insertComanda(new insertComandaListener());
		 m_view.deleteComanda(new deleteComandaListener());
		 m_view.selectComanda(new selectComandaListener());
		 m_view.updateComanda(new updateComandaListener());
	 }
		
	 class butonClientiListener implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			m_view.client.setVisible(true);
		}
		 
	 }
	 class butonComenziListener implements ActionListener{

			public void actionPerformed(ActionEvent arg0) {
				m_view.comenzi.setVisible(true);
			}
			 
		 }
	 class butonProdusListener implements ActionListener{

			public void actionPerformed(ActionEvent arg0) {
				m_view.produse.setVisible(true);
			}
			 
		 }
	 
	 class insertClientListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			String idClientText="",baniClientText="",varstaClientText="";
			idClientText=m_view.getIdclienttext();
			numeClient=m_view.getNumeclienttext();
			adresaClient=m_view.getAdresaclienttext();
			varstaClientText=m_view.getVarstaclienttext();
			baniClientText=m_view.getBaniclienttext();
			emailClient=m_view.getEmailclienttext();
			//System.out.println(idClientText+" "+numeClient+" "+adresaClient+" "+varstaClientText+" "+emailClient+" "+baniClientText);
			try {
				idClient=Integer.parseInt(idClientText);
				varstaClient=Integer.parseInt(varstaClientText);
				baniClient=Integer.parseInt(baniClientText);
			} catch(NumberFormatException e1) {
				m_view.showException("Imput invalid!");
			}
			Client c=new Client(idClient,numeClient,adresaClient,emailClient,varstaClient,baniClient);
			//System.out.println(c.toString());
			try {
			   m_client.insertClient(c);
			} catch (Exception ex) {
				LOGGER.log(Level.INFO, ex.getMessage());
			}
		}
		 
	 }
	 
	 class deleteClientListener implements ActionListener{

			public void actionPerformed(ActionEvent e) {
				String idClientText="";
				idClientText=m_view.getIdclienttext();
				try {
					idClient=Integer.parseInt(idClientText);
				}catch(NumberFormatException e1) {
					m_view.showException("Imput invalid!");
				}
				Client c=new Client();
				try {
					m_client.deleteClient(c,idClient);
				}catch (Exception ex) {
					LOGGER.log(Level.INFO, ex.getMessage());
				}
			}
			 
		 }
	 
	 class updateClientListener implements ActionListener{

			public void actionPerformed(ActionEvent e) {
				String idClientText="",baniClientText="",varstaClientText="";
				idClientText=m_view.getIdclienttext();
				numeClient=m_view.getNumeclienttext();
				adresaClient=m_view.getAdresaclienttext();
				varstaClientText=m_view.getVarstaclienttext();
				baniClientText=m_view.getBaniclienttext();
				emailClient=m_view.getEmailclienttext();
				//System.out.println(idClientText+" "+numeClient+" "+adresaClient+" "+varstaClientText+" "+emailClient+" "+baniClientText);
				try {
					idClient=Integer.parseInt(idClientText);
					varstaClient=Integer.parseInt(varstaClientText);
					baniClient=Integer.parseInt(baniClientText);
				} catch(NumberFormatException e1) {
					m_view.showException("Imput invalid!");
				}
				Client c=new Client(idClient,numeClient,adresaClient,emailClient,varstaClient,baniClient);
				//System.out.println(c.toString());
				try {
				   m_client.updateClient(c,idClient);
				} catch (Exception ex) {
					LOGGER.log(Level.INFO, ex.getMessage());
				}
				
			}
			  
		 }
	 
	 class selectClientListener implements ActionListener{

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				List<Client> Clienti=m_client.findAllClients();
				/*String[] columnsName= {"ID","Name","Address","Email","Age","Money"};
				String[][] data=new String[50][6];
				int i=0;
				for(Client t:Clienti) {
					String[] rowElements=new String[6];
					rowElements[0]=Integer.toString(t.getId());
					rowElements[1]=new String(t.getName());
					rowElements[2]=new String(t.getAddress());
					rowElements[3]=new String(t.getEmail());
					rowElements[4]=Integer.toString(t.getAge());
					rowElements[5]=Integer.toString(t.getMoney());
				data[i]=rowElements;
				i++;
				}
				JFrame frame1=new JFrame("Tabel clienti:");
				JTable clientiTable=new JTable(data,columnsName);
				clientiTable.setCellSelectionEnabled(true);
				clientiTable.setBounds(300,400,200,300);
				JScrollPane sp=new JScrollPane(clientiTable);
				frame1.add(sp);
				frame1.setSize(400,500);
				frame1.setVisible(true);
				*/
				m_view.createTable(Clienti);
			}
			 
		 }
	 
	 class insertProdusListener implements ActionListener{

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String idProdusText="",cantitateProdusText="",pretProdusText="";
				numeProdus=m_view.getNumeprodustext();
				idProdusText=m_view.getIdprodustext();
				cantitateProdusText=m_view.getCantitateprodustext();
				pretProdusText=m_view.getPretprodustext();
				try {
					idProdus=Integer.parseInt(idProdusText);
					cantitateProdus=Integer.parseInt(cantitateProdusText);
					pretProdus=Integer.parseInt(pretProdusText);
				}catch(NumberFormatException e1) {
					m_view.showException("Imput invalid!");
				}
				Produs p=new Produs(idProdus,numeProdus,cantitateProdus,pretProdus);
				m_produs.insertProdus(p);
			}
			 
		 }
		 
		 class deleteProdusListener implements ActionListener{

				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					String idProdusText="";
					idProdusText=m_view.getIdprodustext();
					try {
						idProdus=Integer.parseInt(idProdusText);
					}catch(NumberFormatException e1) {
						m_view.showException("Imput invalid!");
					}
					Produs p=new Produs();
					m_produs.deleteProdus(p, idProdus);
				}
				 
			 }
		 
		 class updateProdusListener implements ActionListener{

				public void actionPerformed(ActionEvent e) {
					String idProdusText="",cantitateProdusText="",pretProdusText="";
					numeProdus=m_view.getNumeprodustext();
					idProdusText=m_view.getIdprodustext();
					cantitateProdusText=m_view.getCantitateprodustext();
					pretProdusText=m_view.getPretprodustext();
					try {
						idProdus=Integer.parseInt(idProdusText);
						cantitateProdus=Integer.parseInt(cantitateProdusText);
						pretProdus=Integer.parseInt(pretProdusText);
					}catch(NumberFormatException e1) {
						m_view.showException("Imput invalid!");
					}
					Produs p=new Produs(idProdus,numeProdus,cantitateProdus,pretProdus);
					m_produs.updateProdus(p,idProdus);
					
				}
				 
			 }
		 
		 class selectProdusListener implements ActionListener{

				public void actionPerformed(ActionEvent e) {
					List<Produs> Produse=m_produs.findAllProducts();
					/*String[] columnsName= {"ID","Name","Cantity","Price per kg"};
					String[][] data=new String[50][4];
					int i=0;
					for(Produs t:Produse) {
						String[] rowElements=new String[6];
						rowElements[0]=Integer.toString(t.getId());
						rowElements[1]=new String(t.getName());
						rowElements[2]=Integer.toString(t.getCantitate());
						rowElements[3]=Integer.toString(t.getPretperkg());
					data[i]=rowElements;
					i++;
					}
					JFrame frame1=new JFrame("Tabel produse:");
					JTable clientiTable=new JTable(data,columnsName);
					clientiTable.setCellSelectionEnabled(true);
					clientiTable.setBounds(300,400,200,300);
					JScrollPane sp=new JScrollPane(clientiTable);
					frame1.add(sp);
					frame1.setSize(400,500);
					frame1.setVisible(true);
					*/
					m_view.createTable(Produse);
				}
				 
			 }
		 
		 class insertComandaListener implements ActionListener{

				public void actionPerformed(ActionEvent arg) {
					// TODO Auto-generated method stub
					String idText="",idClientComText="",idProdusComText="",cantitatetext="";
					idText=m_view.getIdcomandatext();
					idClientComText=m_view.getIdclientcomtext();
					idProdusComText=m_view.getIdproduscomtext().getText();
					cantitatetext=m_view.getCantitatetext();
					
					try {
						id=Integer.parseInt(idText);
						idClientCom=Integer.parseInt(idClientComText);
						idProdusCom=Integer.parseInt(idProdusComText);
						cantitate=Integer.parseInt(cantitatetext);
					}catch(NumberFormatException e1) {
						m_view.showException("Imput invalid!");
					}
					Comanda com=new Comanda(id,idClientCom,idProdusCom);
					m_comanda.insertComanda(com);
					
					
				Produs p=m_produs.findProdusById(idProdusCom);
				Client client=m_client.findClientById(idClientCom);
				
				if(cantitate<p.getCantitate()&& client.getMoney()>cantitate*p.getPretperkg()) {
				 p.setCantitate(p.getCantitate()-cantitate);
				 client.setMoney(client.getMoney()-cantitate*p.getPretperkg());
				 m_produs.updateProdus(p,p.getId());
				 m_client.updateClient(client,client.getId());
				 int valoare=cantitate*p.getPretperkg();
				 try {
						PrintWriter fisierTxt=new PrintWriter("ComandaDeProduse.txt","UTF-8");
						fisierTxt.println("Comanda:");
						fisierTxt.println("Clientul cu id-ul: "+com.getIdClient());
						fisierTxt.println("Produsul comandat are id-ul: "+com.getIdProdus());
						fisierTxt.println("Cantitate comandata:"+cantitate);
						fisierTxt.println("Valoarea comenzii: "+valoare);
					    fisierTxt.close();	
					}catch(Exception e) {
						System.out.println(e.getMessage()+" la scriere in fisier");
					}
				 
				}
				else {
					m_view.showException("Cantitate insuficienta sau bani insuficienti!");
				}
				
					
				
				}
				
				
				 
			 }
			 
			 class deleteComandaListener implements ActionListener{

					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						String idText="";
						idText=m_view.getIdcomandatext();
						try {
							id=Integer.parseInt(idText);
						}catch(NumberFormatException e1) {
							m_view.showException("Imput invalid!");
						}
						Comanda com=new Comanda();
						m_comanda.deleteComanda(com, id);
					}
					 
				 }
			 
			 class updateComandaListener implements ActionListener{

					public void actionPerformed(ActionEvent e) {
						String idText="",idClientComText="",idProdusComText="";
						idText=m_view.getIdcomandatext();
						idClientComText=m_view.getIdclientcomtext();
						idProdusComText=m_view.getIdproduscomtext().getText();
						try {
							id=Integer.parseInt(idText);
							idClientCom=Integer.parseInt(idClientComText);
							idProdusCom=Integer.parseInt(idProdusComText);
						}catch(NumberFormatException e1) {
							m_view.showException("Imput invalid!");
						}
						Comanda com=new Comanda(id,idClientCom,idProdusCom);
						m_comanda.updateComanda(com,id);
						
					}
					 
				 }
			 
			 class selectComandaListener implements ActionListener{

					public void actionPerformed(ActionEvent e) {
						List<Comanda> Comenzi=m_comanda.findAllComands();
						/*String[] columnsName= {"ID","Id Client","Id Produs"};
						String[][] data=new String[50][3];
						int i=0;
						for(Comanda t:Comenzi) {
							String[] rowElements=new String[6];
							rowElements[0]=Integer.toString(t.getId());
							rowElements[1]=Integer.toString(t.getIdClient());
							rowElements[2]=Integer.toString(t.getIdProdus());
						data[i]=rowElements;
						i++;
						}
						JFrame frame1=new JFrame("Tabel comenzi:");
						JTable clientiTable=new JTable(data,columnsName);
						clientiTable.setCellSelectionEnabled(true);
						clientiTable.setBounds(300,400,200,300);
						JScrollPane sp=new JScrollPane(clientiTable);
						frame1.add(sp);
						frame1.setSize(400,500);
						frame1.setVisible(true);
						*/
						m_view.createTable(Comenzi);
					}
					 
				 }
	 
}
