package model;

public class Comanda {
     private int id;
     private int idClient;
     private int idProdus;
	
     
     public Comanda(int id, int idClient, int idProdus) {
		super();
		this.id = id;
		this.idClient = idClient;
		this.idProdus = idProdus;
	}
	public Comanda() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdClient() {
		return idClient;
	}
	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}
	public int getIdProdus() {
		return idProdus;
	}
	public void setIdProdus(int idProdus) {
		this.idProdus = idProdus;
	}
	@Override
	public String toString() {
		return "Comanda [idComanda=" + id + ", idClient=" + idClient + ", idProdus=" + idProdus + "]";
	}
     
     
}
