package model;

public class Produs {
    private int id;
    private String name;
    private int cantitate;
    private int pretperkg;
    
    public Produs() {
    }

	public Produs(int id, String name, int cantitate, int pretperkg) {
		super();
		this.id = id;
		this.name = name;
		this.cantitate = cantitate;
		this.pretperkg = pretperkg;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCantitate() {
		return cantitate;
	}

	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}

	public int getPretperkg() {
		return pretperkg;
	}

	public void setPretperkg(int pretperkg) {
		this.pretperkg = pretperkg;
	}

	@Override
	public String toString() {
		return "Produs [id=" + id + ", name=" + name + ", cantitate=" + cantitate + ", pretperkg=" + pretperkg + "]";
	}
    
    
}
