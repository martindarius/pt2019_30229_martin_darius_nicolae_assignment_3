package start;

import java.sql.SQLException;
import java.util.logging.Logger;

import bll.ClientBLL;
import bll.ComandaBLL;
import bll.ProdusBLL;
import presentation.Controller;
import presentation.View;


public class Start {
	protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());

	public static void main(String[] args) throws SQLException {
        /*
		ClientBLL clientBll = new ClientBLL();
		ProdusBLL produsBLL=new ProdusBLL();
		ComandaBLL comandaBll=new ComandaBLL();
		Produs p=new Produs(1,"carne",10,3);
		Comanda com=new Comanda(130,10,20);
		Client c = new Client(30,"Mihai","dasds","aedas",20,100);
		Client client1 = null;
		List<Client> Clienti;
		
		try {
			//client1 = clientBll.findClientById(1);
			//clientBll.insertClient(c);
			//produsBLL.deleteProdus(p,1);
             // comandaBll.deleteComanda(com,13);
             // System.out.println(Clienti=clientBll.findAllClients());
			//clientBll.deleteClient(c, 10);
		
		} catch (Exception ex) {
			LOGGER.log(Level.INFO, ex.getMessage());
		}
       */
		ClientBLL clientBll = new ClientBLL();
		ProdusBLL produsBLL=new ProdusBLL();
		ComandaBLL comandaBll=new ComandaBLL();
		View view=new View();
		Controller controller=new Controller(clientBll,comandaBll,produsBLL,view);

	}

}
