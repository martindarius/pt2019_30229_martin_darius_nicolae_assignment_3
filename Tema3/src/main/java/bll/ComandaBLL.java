package bll;

import java.io.PrintWriter;
import java.util.List;
import java.util.NoSuchElementException;

import bll.validators.Validator;
import dao.ComandaDAO;
import model.Comanda;

/**
 * @author Dariusika
 *
 */
public class ComandaBLL {
	private List<Validator<Comanda>> validators;
	private ComandaDAO comandaDAO;

	public ComandaBLL() {

		comandaDAO = new ComandaDAO();
	}

	public Comanda findComandById(int id) {
		Comanda st = comandaDAO.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The comand with id =" + id + " was not found!");
		}
		return st;
	}
	
	/**
	 * @param c este un obiect de tip comanda 
	 * Aici se insereaza o comanda in baza de date
	 */
	public void insertComanda(Comanda c) {
		Comanda st=comandaDAO.insert(c);
			
	}
	
	 /**
	 * @param c
	 * @param id
	 */
	public void updateComanda(Comanda c,int id) {
		 Comanda st=comandaDAO.update(c, id);
	   }
	   
	   public void deleteComanda(Comanda c,int id) {
		   Comanda st=comandaDAO.delete(c, id);
	   }
	   
	   public List<Comanda> findAllComands() {
		   List<Comanda> lista=comandaDAO.findAll();
		   return lista;
	   }
}
