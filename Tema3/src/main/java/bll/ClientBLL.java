package bll;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import bll.validators.ClientAgeValidator;
import bll.validators.EmailValidator;
import bll.validators.Validator;
import dao.ClientDAO;
import model.Client;



public class ClientBLL {
	private List<Validator<Client>> validators;
	private ClientDAO clientDAO;

	public ClientBLL() {
		validators = new ArrayList<Validator<Client>>();
		validators.add(new EmailValidator());
		validators.add(new ClientAgeValidator());

		clientDAO = new ClientDAO();
	}
	



	/**
	 * @param id pentru a stii ce client cautam
	 */
	public Client findClientById(int id) {
		Client st = clientDAO.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The client with id =" + id + " was not found!");
		}
		return st;
	}
	
	/**
	 * @param c este un obiect de tipul client 
	 * Aici inseram un client in baza de date.
	 */
	public void insertClient(Client c) {
		Client st=clientDAO.insert(c);
		
	}
   /**
 * @param c este un obiect de tipul client 
 * @param id pentru a stii ce client updatam
 * Aici updatam un client in baza de date.
 */
	public void updateClient(Client c,int id) {
	   Client st=clientDAO.update(c, id);
   }
	 /**
	 * @param c este un obiect de tipul client 
	 * @param id pentru a stii ce client stergem
	 * Aici stergem un client in baza de date.
	 */
   public void deleteClient(Client c,int id) {
	   Client st=clientDAO.delete(c, id);
   }
   
   /**
 * @return o lista de clienti pe care o preia din baza de date
 */
public List<Client> findAllClients() {
	   List<Client> lista=clientDAO.findAll();
	   return lista;
   }
}
