package presentation;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

public class View {
	JFrame principal = new JFrame("Principal");
	JFrame client = new JFrame("Client");
	JFrame produse = new JFrame("Produse");
	JFrame comenzi = new JFrame("Comenzi");
	JFrame tabelclient = new JFrame("Tabel clienti");
	JFrame tabelprodus = new JFrame("Tabel produse");
	JFrame tabelcomanda = new JFrame("Tabel comenzi");

	JPanel pr1 = new JPanel();
	JPanel pr2 = new JPanel();
	JPanel pr3 = new JPanel();
	JButton produsebutton = new JButton("Produse");
	JButton clientibutton = new JButton("Clienti");
	JButton comenzibutton = new JButton("Comenzi");
	JLabel alegeti = new JLabel("Alegeti pe ce tabel vreti sa faceti o operatiune:");

	JPanel c1 = new JPanel();
	JPanel c2 = new JPanel();
	JPanel c3 = new JPanel();

	JLabel idclient = new JLabel("ID:");
	JLabel numeclient = new JLabel("NUME:");
	JLabel varstaclient = new JLabel("VARSTA:");
	JLabel adresaclient = new JLabel("ADRESA:");
	JLabel emailclient = new JLabel("EMAIL:");
	JLabel baniclient = new JLabel("BANI:");
	JTextField idclienttext = new JTextField(5);
	JTextField numeclienttext = new JTextField(30);
	JTextField varstaclienttext = new JTextField(5);
	JTextField adresaclienttext = new JTextField(50);
	JTextField emailclienttext = new JTextField(30);
	JTextField baniclienttext = new JTextField(10);

	JButton insertClient = new JButton("Insert");
	JButton deleteClient = new JButton("Delete");
	JButton updateClient = new JButton("Update");
	JButton selectClient = new JButton("Show All");

	JPanel p1 = new JPanel();
	JPanel p2 = new JPanel();
	JPanel p3 = new JPanel();
	JButton insertProdus = new JButton("Insert");
	JButton deleteProdus = new JButton("Delete");
	JButton updateProdus = new JButton("Update");
	JButton selectProdus = new JButton("Show All");
	JLabel idprodus = new JLabel("ID:");
	JLabel numeprodus = new JLabel("NUME:");
	JLabel cantitateprodus = new JLabel("CANTITATE:");
	JLabel pretprodus = new JLabel("Pret/kg:");
	JTextField idprodustext = new JTextField(5);
	JTextField numeprodustext = new JTextField(20);
	JTextField cantitateprodustext = new JTextField(5);
	JTextField pretprodustext = new JTextField(5);

	JPanel com1 = new JPanel();
	JPanel com2 = new JPanel();
	JPanel com3 = new JPanel();
	JButton insertComanda = new JButton("Insert");
	JButton deleteComanda = new JButton("Delete");
	JButton updateComanda = new JButton("Update");
	JButton selectComanda = new JButton("Show All");
	JLabel idcomanda = new JLabel("ID:");
	JLabel idproduscom = new JLabel("ID produs:");
	JLabel idclientcom = new JLabel("ID client:");
	JLabel cantitate = new JLabel("Cantitate:");
	JTextField cantitatetext = new JTextField(5);
	JTextField idcomandatext = new JTextField(5);
	JTextField idproduscomtext = new JTextField(5);
	JTextField idclientcomtext = new JTextField(5);

	public View() {
		pr1.add(produsebutton);
		pr1.add(clientibutton);
		pr1.add(comenzibutton);
		pr1.setLayout(new FlowLayout());
		pr2.add(alegeti);
		principal.add(pr2, BorderLayout.PAGE_START);
		principal.add(pr1, BorderLayout.CENTER);
		principal.setSize(400, 200);
		principal.setVisible(true);

		c1.add(insertClient);
		c1.add(updateClient);
		c1.add(deleteClient);
		c1.add(selectClient);
		c2.add(idclient);
		c2.add(idclienttext);
		c2.add(numeclient);
		c2.add(numeclienttext);
		c2.add(adresaclient);
		c2.add(adresaclienttext);
		c2.add(varstaclient);
		c2.add(varstaclienttext);
		c2.add(emailclient);
		c2.add(emailclienttext);
		c2.add(baniclient);
		c2.add(baniclienttext);
		c2.setLayout(new GridLayout(6, 2));
		c1.setLayout(new FlowLayout());
		client.add(c2, BorderLayout.PAGE_START);
		client.add(c1, BorderLayout.PAGE_END);
		client.setSize(500, 400);
		client.setVisible(false);

		p1.add(insertProdus);
		p1.add(updateProdus);
		p1.add(deleteProdus);
		p1.add(selectProdus);
		p1.setLayout(new FlowLayout());
		p2.add(idprodus);
		p2.add(idprodustext);
		p2.add(numeprodus);
		p2.add(numeprodustext);
		p2.add(cantitateprodus);
		p2.add(cantitateprodustext);
		p2.add(pretprodus);
		p2.add(pretprodustext);
		p2.setLayout(new GridLayout(4, 2));
		produse.add(p2, BorderLayout.PAGE_START);
		produse.add(p1, BorderLayout.PAGE_END);
		produse.setSize(500, 400);
		produse.setVisible(false);

		com1.add(insertComanda);
		com1.add(deleteComanda);
		com1.add(updateComanda);
		com1.add(selectComanda);
		com1.setLayout(new FlowLayout());
		com2.add(idcomanda);
		com2.add(idcomandatext);
		com2.add(idclientcom);
		com2.add(idclientcomtext);
		com2.add(idproduscom);
		com2.add(idproduscomtext);
		com2.add(cantitate);
		com2.add(cantitatetext);
		com2.setLayout(new GridLayout(4, 2));
		comenzi.add(com1, BorderLayout.PAGE_END);
		comenzi.add(com2, BorderLayout.PAGE_START);
		comenzi.setSize(500, 400);
		comenzi.setVisible(false);

	}

	public <T> void createTable(List<T> list) {
		Class clasa = list.getClass();
		Iterator<T> it = list.iterator();
		Field[] f = null;
		Object obj = null;
		Object[] values = null;
		String[] columnsName = null;
		String[] rows = null;
		
		int i = 0;
		int j = 0;
		int ok = 0;
		if (list != null)
			f = list.get(0).getClass().getDeclaredFields();
		String[][] data = new String[list.size()][f.length];
		columnsName = new String[f.length];
		values = new Object[f.length];
		rows = new String[f.length];
		while (it.hasNext()) {
			Object temp = it.next();
			for (Field field : f) {
				field.setAccessible(true);

					columnsName[i] = field.getName();

					// System.out.println(columnsName[i]);
					try {
						obj = field.get(temp);
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					try {
						values[i] = field.get(temp);
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					rows[i] = values[i].toString();
					i++;
			 
			}

			for (int k = 0; k < f.length; k++) {
				data[j][k] = rows[k];
				
			}
			j++;
			i=0;
		}
		System.out.println(columnsName[0] + " " + columnsName[2]);
		// System.out.println(data[1][0]);
		JFrame frame1 = new JFrame();
		JTable clientiTable = new JTable(data, columnsName);
		clientiTable.setCellSelectionEnabled(true);
		clientiTable.setBounds(300, 400, 200, 300);
		JScrollPane sp = new JScrollPane(clientiTable);
		frame1.add(sp);
		frame1.setSize(400, 500);
		frame1.setVisible(true);

	}

	public JFrame getPrincipal() {
		return principal;
	}

	public void setPrincipal(JFrame principal) {
		this.principal = principal;
	}

	public JFrame getClient() {
		return client;
	}

	public void setClient(JFrame client) {
		this.client = client;
	}

	public JFrame getProduse() {
		return produse;
	}

	public void setProduse(JFrame produse) {
		this.produse = produse;
	}

	public JFrame getComenzi() {
		return comenzi;
	}

	public void setComenzi(JFrame comenzi) {
		this.comenzi = comenzi;
	}

	public String getCantitatetext() {
		return cantitatetext.getText();
	}

	public void setCantitatetext(JTextField cantitatetext) {
		this.cantitatetext = cantitatetext;
	}

	public JButton getProdusebutton() {
		return produsebutton;
	}

	public void setProdusebutton(JButton produsebutton) {
		this.produsebutton = produsebutton;
	}

	public JButton getClientibutton() {
		return clientibutton;
	}

	public void setClientibutton(JButton clientibutton) {
		this.clientibutton = clientibutton;
	}

	public JButton getComenzibutton() {
		return comenzibutton;
	}

	public void setComenzibutton(JButton comenzibutton) {
		this.comenzibutton = comenzibutton;
	}

	public JLabel getAlegeti() {
		return alegeti;
	}

	public void setAlegeti(JLabel alegeti) {
		this.alegeti = alegeti;
	}

	public JLabel getIdclient() {
		return idclient;
	}

	public void setIdclient(JLabel idclient) {
		this.idclient = idclient;
	}

	public JLabel getNumeclient() {
		return numeclient;
	}

	public void setNumeclient(JLabel numeclient) {
		this.numeclient = numeclient;
	}

	public JLabel getVarstaclient() {
		return varstaclient;
	}

	public void setVarstaclient(JLabel varstaclient) {
		this.varstaclient = varstaclient;
	}

	public JLabel getAdresaclient() {
		return adresaclient;
	}

	public void setAdresaclient(JLabel adresaclient) {
		this.adresaclient = adresaclient;
	}

	public JLabel getEmailclient() {
		return emailclient;
	}

	public void setEmailclient(JLabel emailclient) {
		this.emailclient = emailclient;
	}

	public JLabel getBaniclient() {
		return baniclient;
	}

	public void setBaniclient(JLabel baniclient) {
		this.baniclient = baniclient;
	}

	public String getIdclienttext() {
		return idclienttext.getText();
	}

	public void setIdclienttext(JTextField idclienttext) {
		this.idclienttext = idclienttext;
	}

	public String getNumeclienttext() {
		return numeclienttext.getText();
	}

	public void setNumeclienttext(JTextField numeclienttext) {
		this.numeclienttext = numeclienttext;
	}

	public String getVarstaclienttext() {
		return varstaclienttext.getText();
	}

	public void setVarstaclienttext(JTextField varstaclienttext) {
		this.varstaclienttext = varstaclienttext;
	}

	public String getAdresaclienttext() {
		return adresaclienttext.getText();
	}

	public void setAdresaclienttext(JTextField adresaclienttext) {
		this.adresaclienttext = adresaclienttext;
	}

	public String getEmailclienttext() {
		return emailclienttext.getText();
	}

	public void setEmailclienttext(JTextField emailclienttext) {
		this.emailclienttext = emailclienttext;
	}

	public String getBaniclienttext() {
		return baniclienttext.getText();
	}

	public void setBaniclienttext(JTextField baniclienttext) {
		this.baniclienttext = baniclienttext;
	}

	public JButton getInsertClient() {
		return insertClient;
	}

	public void setInsertClient(JButton insertClient) {
		this.insertClient = insertClient;
	}

	public JButton getDeleteClient() {
		return deleteClient;
	}

	public void setDeleteClient(JButton deleteClient) {
		this.deleteClient = deleteClient;
	}

	public JButton getUpdateClient() {
		return updateClient;
	}

	public void setUpdateClient(JButton updateClient) {
		this.updateClient = updateClient;
	}

	public JButton getSelectClient() {
		return selectClient;
	}

	public void setSelectClient(JButton selectClient) {
		this.selectClient = selectClient;
	}

	public JButton getInsertProdus() {
		return insertProdus;
	}

	public void setInsertProdus(JButton insertProdus) {
		this.insertProdus = insertProdus;
	}

	public JButton getDeleteProdus() {
		return deleteProdus;
	}

	public void setDeleteProdus(JButton deleteProdus) {
		this.deleteProdus = deleteProdus;
	}

	public JButton getUpdateProdus() {
		return updateProdus;
	}

	public void setUpdateProdus(JButton updateProdus) {
		this.updateProdus = updateProdus;
	}

	public JButton getSelectProdus() {
		return selectProdus;
	}

	public void setSelectProdus(JButton selectProdus) {
		this.selectProdus = selectProdus;
	}

	public JLabel getIdprodus() {
		return idprodus;
	}

	public void setIdprodus(JLabel idprodus) {
		this.idprodus = idprodus;
	}

	public JLabel getNumeprodus() {
		return numeprodus;
	}

	public void setNumeprodus(JLabel numeprodus) {
		this.numeprodus = numeprodus;
	}

	public JLabel getCantitateprodus() {
		return cantitateprodus;
	}

	public void setCantitateprodus(JLabel cantitateprodus) {
		this.cantitateprodus = cantitateprodus;
	}

	public JLabel getPretprodus() {
		return pretprodus;
	}

	public void setPretprodus(JLabel pretprodus) {
		this.pretprodus = pretprodus;
	}

	public String getIdprodustext() {
		return idprodustext.getText();
	}

	public void setIdprodustext(JTextField idprodustext) {
		this.idprodustext = idprodustext;
	}

	public String getNumeprodustext() {
		return numeprodustext.getText();
	}

	public void setNumeprodustext(JTextField numeprodustext) {
		this.numeprodustext = numeprodustext;
	}

	public String getCantitateprodustext() {
		return cantitateprodustext.getText();
	}

	public void setCantitateprodustext(JTextField cantitateprodustext) {
		this.cantitateprodustext = cantitateprodustext;
	}

	public String getPretprodustext() {
		return pretprodustext.getText();
	}

	public void setPretprodustext(JTextField pretprodustext) {
		this.pretprodustext = pretprodustext;
	}

	public JButton getInsertComanda() {
		return insertComanda;
	}

	public void setInsertComanda(JButton insertComanda) {
		this.insertComanda = insertComanda;
	}

	public JButton getDeleteComanda() {
		return deleteComanda;
	}

	public void setDeleteComanda(JButton deleteComanda) {
		this.deleteComanda = deleteComanda;
	}

	public JButton getUpdateComanda() {
		return updateComanda;
	}

	public void setUpdateComanda(JButton updateComanda) {
		this.updateComanda = updateComanda;
	}

	public JButton getSelectComanda() {
		return selectComanda;
	}

	public void setSelectComanda(JButton selectComanda) {
		this.selectComanda = selectComanda;
	}

	public JLabel getIdcomanda() {
		return idcomanda;
	}

	public void setIdcomanda(JLabel idcomanda) {
		this.idcomanda = idcomanda;
	}

	public JLabel getIdproduscom() {
		return idproduscom;
	}

	public void setIdproduscom(JLabel idproduscom) {
		this.idproduscom = idproduscom;
	}

	public JLabel getIdclientcom() {
		return idclientcom;
	}

	public void setIdclientcom(JLabel idclientcom) {
		this.idclientcom = idclientcom;
	}

	public String getIdcomandatext() {
		return idcomandatext.getText();
	}

	public void setIdcomandatext(JTextField idcomandatext) {
		this.idcomandatext = idcomandatext;
	}

	public JTextField getIdproduscomtext() {
		return idproduscomtext;
	}

	public void setIdproduscomtext(JTextField idproduscomtext) {
		this.idproduscomtext = idproduscomtext;
	}

	public String getIdclientcomtext() {
		return idclientcomtext.getText();
	}

	public void setIdclientcomtext(JTextField idclientcomtext) {
		this.idclientcomtext = idclientcomtext;
	}

	public void butonProdus(ActionListener e) {
		produsebutton.addActionListener(e);
	}

	public void butonClienti(ActionListener e) {
		clientibutton.addActionListener(e);
	}

	public void butonComenzi(ActionListener e) {
		comenzibutton.addActionListener(e);
	}

	public void insertClient(ActionListener e) {
		insertClient.addActionListener(e);
	}

	public void deleteClient(ActionListener e) {
		deleteClient.addActionListener(e);
	}

	public void updateClient(ActionListener e) {
		updateClient.addActionListener(e);
	}

	public void selectClient(ActionListener e) {
		selectClient.addActionListener(e);
	}

	public void insertProdus(ActionListener e) {
		insertProdus.addActionListener(e);
	}

	public void deleteProdus(ActionListener e) {
		deleteProdus.addActionListener(e);
	}

	public void updateProdus(ActionListener e) {
		updateProdus.addActionListener(e);
	}

	public void selectProdus(ActionListener e) {
		selectProdus.addActionListener(e);
	}

	public void insertComanda(ActionListener e) {
		insertComanda.addActionListener(e);
	}

	public void deleteComanda(ActionListener e) {
		deleteComanda.addActionListener(e);
	}

	public void updateComanda(ActionListener e) {
		updateComanda.addActionListener(e);
	}

	public void selectComanda(ActionListener e) {
		selectComanda.addActionListener(e);
	}

	public void showException(String mesaj) {
		JOptionPane.showMessageDialog(client, mesaj);
	}

}
