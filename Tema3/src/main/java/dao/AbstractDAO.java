package dao;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 * @Source http://www.java-blog.com/mapping-javaobjects-database-reflection-generics
 */
public class AbstractDAO<T> {
	protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

	private final Class<T> type;

	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

	}

	private String createSelectQuery(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" *");
		sb.append(" FROM ");
		sb.append(type.getSimpleName());
		sb.append(" WHERE " + field + " =?");
		return sb.toString();
	}

	private String createInsertQuery() {
		String querry = "INSERT INTO " + type.getSimpleName() + " VALUES(";
		for (int i = 0; i < type.getDeclaredFields().length; i++) {
			querry += "?,";
		}
		querry = querry.substring(0, querry.length() - 1);
		querry += ");";
		return querry;
	}

	public String createUpdateQuery(int id) {
		String querry = "UPDATE " + type.getSimpleName() + " SET ";
		for (int i = 0; i < type.getDeclaredFields().length; i++) {
			Field[] x = type.getDeclaredFields();
			querry += x[i].getName() + "=?, ";
		}
		querry = querry.substring(0, querry.length() - 2);
		querry += " WHERE id=" + id;
		System.out.println(querry);
		return querry;
	}

	public String createFindAllQuery() {
		String querry="SELECT * FROM ";
		querry+=type.getSimpleName();
		return querry;
	}
	
	public List<T> findAll() {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createFindAllQuery();
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery();

			return createObjects(resultSet);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findAll " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	public T findById(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("id");
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			return createObjects(resultSet).get(0);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	private List<T> createObjects(ResultSet resultSet) {
		List<T> list = new ArrayList<T>();

		try {
			while (resultSet.next()) {
				T instance = type.newInstance();
				for (Field field : type.getDeclaredFields()) {
					Object value = resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value);
				}
				list.add(instance);
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		return list;
	}

	public T insert(T t) {
		Connection connection = null;
		PreparedStatement insert = null;
		ResultSet resultSet = null;
		String query = createInsertQuery();
		System.out.println(query);
		try {
			connection = ConnectionFactory.getConnection();
			insert = connection.prepareStatement(query);
			int i = 1;
			insert = connection.prepareStatement(createInsertQuery());
			for (Field field : t.getClass().getDeclaredFields()) {
				field.setAccessible(true);
				if (field.getType() == String.class) {
					try {
						insert.setString(i, (String) field.get(t));
						i++;
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					try {
						insert.setInt(i, (Integer) field.get(t));
						i++;
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			insert.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

			LOGGER.log(Level.WARNING, type.getName() + "DAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(insert);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	public T update(T t, int id) {
		Connection connection = null;
		PreparedStatement update = null;
		ResultSet resultSet = null;
		String query = createUpdateQuery(id);
		System.out.println(query);
		try {
			connection = ConnectionFactory.getConnection();
			update = connection.prepareStatement(query);
			int i = 1;
			update = connection.prepareStatement(createUpdateQuery(id));
			for (Field field : t.getClass().getDeclaredFields()) {

				field.setAccessible(true);
				if (field.getType() == String.class) {
					try {
						update.setString(i, (String) field.get(t));
						i++;
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					try {
						update.setInt(i, (Integer) field.get(t));
						i++;
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				if (i == 1) {

					update.setInt(1, id);
				}
			}
			update.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();

			LOGGER.log(Level.WARNING, type.getName() + "DAO:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(update);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	public String createDeleteQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("DELETE ");
		sb.append(" FROM ");
		sb.append(type.getSimpleName());
		sb.append(" WHERE id=?"); // ?
		return sb.toString();
	}

	public T delete(T t, int id) {
		Connection connection = null;
		PreparedStatement delete = null;
		ResultSet resultSet = null;
		String query = createDeleteQuery();
		System.out.println(query);
		try {
			connection = ConnectionFactory.getConnection();
			delete = connection.prepareStatement(query);
			delete = connection.prepareStatement(createDeleteQuery());
			delete.setInt(1, id);

			delete.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

			LOGGER.log(Level.WARNING, type.getName() + "DAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(delete);
			ConnectionFactory.close(connection);
		}
		return null;
	}
}
